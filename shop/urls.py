from django.urls import path, include
from shop import views


urlpatterns = [
    path('all_products/', views.AllProductsAPIView.as_view()),
    path('all_check_filters/', views.AllCheckFiltersAPIView.as_view()),
    path('price_range/', views.GetPriceRangeForPriceFilter.as_view()),
    path('products/<int:pk>', views.ProductDetailsAPIView.as_view())

]