from django.db.models import Min, Max

from rest_framework.views import APIView
from rest_framework.response import Response

from shop.serializers import ProductSerializer, CategorySerializer, MetalSerializer
from shop.models import Product, Category, Metal



class AllCheckFiltersAPIView(APIView):
    def get(self, request):
        categories = Category.objects.all()
        metals = Metal.objects.all()

        category_serializer = CategorySerializer(categories, many=True)
        metal_serializer = MetalSerializer(metals, many=True)

        response = [
            {'type': 'Category', 'payload': category_serializer.data},
            {'type': 'Metal', 'payload': metal_serializer.data}
        ]

        return Response(response)


class GetPriceRangeForPriceFilter(APIView):
    def get(self, request):
        min_product_price = Product.objects.all().aggregate(Min('price')).get('price__min')
        max_product_price = Product.objects.all().aggregate(Max('price')).get('price__max')

        response = {
            'min_product_price': min_product_price,
            'max_product_price': max_product_price,
        }

        return Response(response)


class AllProductsAPIView(APIView):
    def get(self, request, format=None):
        products = Product.objects.all()
        serializer = ProductSerializer(products, many=True)

        return Response(serializer.data)


class ProductDetailsAPIView(APIView):
    @staticmethod
    def _get_object(pk):
        try:
            return Product.objects.get(pk=pk)
        except Product.DoesNotExist:
            return None

    def get(self, request, pk):
        product = self._get_object(pk)

        if product is not None:
            serializer = ProductSerializer(product)

            return Response(serializer.data)
        else:
            return Response({'Error': 'Object does not exist'})


class FilteredProductsAPIView(APIView):
    def get(self, request, format=None):
        print(request)