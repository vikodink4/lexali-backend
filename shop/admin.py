from django.contrib import admin
from shop.models import Category, Product, AdditionalImage, MainImage, Metal


class AdditionalImageInline(admin.StackedInline):
    model = AdditionalImage


class MainImageInline(admin.StackedInline):
    model = MainImage


class ProductAdmin(admin.ModelAdmin):

    inlines = [
        AdditionalImageInline,
        MainImageInline
    ]


admin.site.register(Product, ProductAdmin,)
admin.site.register(Category)
admin.site.register(Metal)



