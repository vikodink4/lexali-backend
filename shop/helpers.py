import os
import uuid


def get_main_image_file_path(instance, filename: str):
    ext = filename.split('.')[-1]
    filename = '{}.{}'.format(uuid.uuid4(), ext)
    product_id = str(instance.product.pk)
    filepath = os.path.join('main_images/{}/'.format(product_id), filename)

    return filepath


def get_additional_images_file_path(instance, filename: str):
    ext = filename.split('.')[-1]
    filename = '{}.{}'.format(uuid.uuid4(), ext)
    product_id = str(instance.product.pk)
    filepath = os.path.join('additional_images/{}/'.format(product_id), filename)

    return filepath





