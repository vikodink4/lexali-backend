from shop.models import Product, Category, MainImage, AdditionalImage, Metal
from rest_framework import serializers


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('pk', 'name')


class MetalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Metal
        fields = ('pk', 'name')


class MainImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = MainImage
        fields = ('pk', 'url')


class AdditionalImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = AdditionalImage
        fields = ('pk', 'url')


class ProductSerializer(serializers.ModelSerializer):
    category = CategorySerializer(read_only=True)
    main_image = MainImageSerializer(read_only=True)
    additional_images = AdditionalImageSerializer(many=True)

    class Meta:
        model = Product
        fields = ('pk', 'name', 'price', 'description', 'category', 'main_image', 'additional_images')
