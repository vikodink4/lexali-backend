from django.db import models
from shop.helpers import get_main_image_file_path, get_additional_images_file_path


class Category(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name


class Metal(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=30)
    price = models.FloatField()
    description = models.CharField(max_length=255)
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True)
    metal = models.ForeignKey(Metal, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.name


class AdditionalImage(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='additional_images')
    url = models.ImageField(upload_to=get_additional_images_file_path, default=None)


class MainImage(models.Model):
    product = models.OneToOneField(Product, on_delete=models.CASCADE, related_name='main_image')
    url = models.ImageField(upload_to=get_main_image_file_path, null=False)
